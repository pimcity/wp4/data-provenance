FROM maven:3-jdk-8-alpine

VOLUME /tmp
COPY target/data-provenance-0.0.1-SNAPSHOT.jar data-provenance-app.jar
CMD ["/usr/bin/java","-Djava.security.egd=file:/dev/./urandom","-jar","/data-provenance-app.jar", "--server.port=8090"]
EXPOSE 8090/tcp
