Data Provenance (DP) OpenAPI
==============================

# Intro

The Data Provenance (DP) is a data management tool to watermark sensitive data as user web browsing history while accounting for user data ownership. It implements algorithms from the database watermarking literature (e.g., [VLDB][ref1]) and aims to bring new research into the area in order to use it in real world data management use cases as ours. We focus in web browsing data, namely URLs, which are a valuable piece of information about user's preferences and behavior, yet not monetizable by data owners in a decentralized manner in the real world yet (only centralized companies as Comscore exist for that). Therefore, out tool opens a new possibility to users to sell watermarked data with the support of the Trading Engine component (out of scope in this demo and intro) so that users just need to rely on REST-based APIs or a web interface to control their data ownership. Thanks to the REST API endpoints, the DP tool can be accessed also by other components of the [PDK][ref4]. Internally, it uses the SpringBoot framework and will store user data on a secure PostgreSQL database as well as decentralized storage thanks to the support of IPFS (InterPlanetary File System) as middleware. Note, in the future watermarked datasets will be encrypted with the appropriate public and or private keys, but that is out of the scope for now. The Web interface is provided by the Swagger OpenAPI tools in our deployment as a single page application.

![Architecture](docs/architecture.png)


# Installation

The DP OpenAPI is written in Java and the [pom.xml] file in the project contains the list of libraries dependencies we require at the application level. For the API to run you also need the following system dependencies for some of such libraries:

## Dependencies
- You will need openjdk11 and both the JAVA_HOME AND JAVA_OPTS configured according to your system, in our case:

	JAVA_OPTS = -DJava net.preferIPv4Stack=true -Djava.net.preferIPv6Addresses=false
	JAVA_HOME = /usr/lib/jvm/java-11-openjdk-amd64

- Then, install maven, ipfsv0.4.16 and postgres at the very least.

- Start ipfs:
```
ipfs daemon

Initializing daemon...

go-ipfs version: 0.4.23-

Repo version: 7

System version: amd64/darwin

Golang version: go1.13.7
```
- The travis.yml file provices a self-contained setup of dependencies if you use CI/CD in a platform as github or gitlab. 

- An instance of [Postgres12+][ref5] and a running service of [IPFS][ref6] and mandatory without IPFS. psql (PostgreSQL) 12.8 should also be installed and running at command line.

- Note: If you run into garbage collection problems with IPFS, you may need:

`ipfs pin ls --type recursive | cut -d' ' -f1 | xargs -n1 ipfs pin rm`

then optionally run storage garbage collection to actually remove things:

`ipfs repo gc`

# Usage Examples

## Starting the API on a server
- Configure [application.properties] accordingly fine and the OpenApiConfig.java file if you change server address in the config package.
- To compile the OpenAPI as a .jar run the following: 
```
$ mvn spring-boot:run
```
- Once it compiles correctly without errors the API is running locally as a local demonstrator, it should be also easy to test locally at: http://localhost:8090/swagger-ui.html

## Using the API on the server
- Go the easypims Data Provenance site: https://easypims.pimcity-h2020.eu/intro-provenance.html
- Try out the online demonstrator: https://easypims.pimcity-h2020.eu/provenance/swagger-ui/index.html?url=/provenance/docs

![System Design](docs/system-design.png)

Your main operating endpoints are:
- POST: /ipfs/dataset For creating new file with urls, one per file line)
- POST: /ipfs/dataset/{queryId} For sending the IPFS hash of a file already in IPFS to the watermarking algorithm.
- GET:  /dp​/wm​/{datasetId} Gets a WM Dataset by IPFS hash (future use, instead of manual inspection using new IPFS hash).

- For a sample text file with urls:

`http://www.marca.es`

`https://www.nhl.com`

`https://ethsat.com`

- You will get back an IPFS hash you must use back again to retrieve the watermarked file content:

`HTTP://WWw.MARca.Es`

`HtTps://www.nHL.Com`

`HttPs://ethsAT.coM`


# Changes
## v0.0.4-SNAPSHOT with added Authentication to involve Oauth2 in neccesary endpoints of the system design above.

## Changes
- Commit [7dd15d5164b09ea007f2476e0ab0b67893a25fd6](https://gitlab.com/pimcity/wp4/data-provenance/-/commit/7dd15d5164b09ea007f2476e0ab0b67893a25fd6): Upload json and yaml with security on. Signed-off-by: agr <alvaro.garcia@imdea.org>
- Commit [f74d79f3e4e0325281f8196101799885ede44684](https://gitlab.com/pimcity/wp4/data-provenance/-/commit/f74d79f3e4e0325281f8196101799885ede44684): Complete security_auth with authCode. Signed-off-by: agr <alvaro.garcia@imdea.org>  Mon Jan 10 15:22:43 2022 +0100
- Commit [a8c5d4445713fac40da7265e0ecee6869b4fefee](https://gitlab.com/pimcity/wp4/data-provenance/-/commit/a8c5d4445713fac40da7265e0ecee6869b4fefee): Attempt to add SwaggerConfig for Oauth. Signed-off-by: agr <alvaro.garcia@imdea.org> Tue Nov 2 12:16:04 2021 +0100
- Commit [32dbec5bb75aa6865d18f56a9df3fba51a658fef](https://gitlab.com/pimcity/wp4/data-provenance/-/commit/32dbec5bb75aa6865d18f56a9df3fba51a658fef): Attempt to add dependencies to pom. Signed-off-by: agr <alvaro.garcia@imdea.org> Mon Oct 25 10:29:26 2021 +0200

# Disclaimer
The high level system was presented in deliverables [D4.1][ref2] and [D4.2][ref3] of PIMCity (subject to future development changes and production). This is an alpha OpenAPI product intented to showcase the capabilities of our algorithms and API to watermark a file with a given list of urls in the format that follows in sample_urls.txt example file here in folder /datasets. For future questions contact us. This is an experimental prototype of the OpenAPI specification we are developing (Alpha version).

# License
![GPLv3 license](https://www.gnu.org/graphics/gplv3-or-later.png) See it in LICENSE.md file of this repository.

[![](https://cdn.rawgit.com/jbenet/contribute-ipfs-gif/master/img/contribute.gif)]

This repository falls under the IPFS [Code of Conduct](https://github.com/ipfs/community/blob/master/code-of-conduct.md).


[ref1]: http://www.vldb.org/conf/2002/S05P03.pdf
[ref2]: https://www.pimcity-h2020.eu/app/uploads/2020/12/D4.1-Design-for-tools-for-improved-data-management.pdf
[ref3]: https://www.pimcity-h2020.eu/app/uploads/2021/10/D4.2_Rev2_Final-design-and-preliminary-version-of-the-tools-for-improved-data-management.pdf
[ref4]: https://easypims.pimcity-h2020.eu/
[ref5]: https://www.postgresql.org/
[ref6]: https://ipfs.io/
