#!/bin/bash

docker stop provenance-api
docker rm provenance-api
docker rmi provenance-api

docker build -t provenance-api .
docker run -d --name provenance-api -p 8090:8090 provenance-api:latest
docker logs provenance-api