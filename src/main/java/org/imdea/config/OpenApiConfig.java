package org.imdea.config;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.OAuthScope;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.*;


@OpenAPIDefinition(info = @Info(title = "Data Provenance",
		description = "Data Provenance REST API documentation", version = "alpha0.4"))
@SecurityScheme(name = "security_auth", type = SecuritySchemeType.OAUTH2,
		flows = @OAuthFlows(authorizationCode = @OAuthFlow(
				authorizationUrl = "https://easypims.pimcity-h2020.eu/oauth/auth"
				, tokenUrl = "https://easypims.pimcity-h2020.eu/oauth/token", scopes = {
				@OAuthScope(name = "read:provenance", description = "read scope"),
				@OAuthScope(name = "create:provenance", description = "write scope") })))
@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI customConfiguration() {
        return new OpenAPI()
        		.addServersItem(new Server().url("https://easypims.pimcity-h2020.eu/provenance"))
                .components(new Components())
                .info(new io.swagger.v3.oas.models.info.Info().title("Data Provenance API Docs").version("alpha0.4")
                        .description("Sample REST API documentation"));
    }
}

