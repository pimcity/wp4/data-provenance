package org.imdea.config;

import java.util.Arrays;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Value;

import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
public class SwaggerConfiguration {

	@Value("${keycloak.auth-server-url}")
	private String AUTH_SERVER = "https://easypims.pimcity-h2020.eu/oauth";

	@Value("${keycloak.credentials.secret}")
	private String CLIENT_SECRET = ""; 

	@Value("${keycloak.resource}")
	private String CLIENT_ID = "provenance";

	@Value("${keycloak.realm}")
	private String REALM = "pimcity";

	private static final String OAUTH_NAME = "spring_oauth";
	private static final String ALLOWED_PATHS = "/src/main/java/org/imdea/config/controller.*";
	private static final String GROUP_NAME = "provenance-api";
	private static final String TITLE = "API Documentation for XXXXXXX Application";
	private static final String DESCRIPTION = "Description here";
	private static final String VERSION = "3.0";


	@Bean public Docket api() { return new
			Docket(DocumentationType.SWAGGER_2)
			.groupName(GROUP_NAME)
			.useDefaultResponseMessages(true)
			.apiInfo(apiInfo())
			.select()
			.apis(RequestHandlerSelectors.any()) .paths(PathSelectors.any()).build()
			.securitySchemes(Arrays.asList(securityScheme()))
			.securityContexts(Arrays.asList(securityContext())); }


	private ApiInfo apiInfo() {
		return new 
				ApiInfoBuilder().title(TITLE).description(DESCRIPTION).version(VERSION).build();
	}

	@Bean
	public SecurityConfiguration security() {
		return SecurityConfigurationBuilder.builder()
				.realm(REALM)
				.clientId(CLIENT_ID)
				.clientSecret(CLIENT_SECRET)
				.appName(GROUP_NAME)
				.scopeSeparator(" ")
				.build();
	}

	private SecurityScheme securityScheme() {
		GrantType grantType =
				new AuthorizationCodeGrantBuilder()
				.tokenEndpoint(new TokenEndpoint(AUTH_SERVER + "/realms/" + REALM + "/protocol/openid-connect/token", GROUP_NAME))
				.tokenRequestEndpoint(
						new TokenRequestEndpoint(AUTH_SERVER + "/realms/" + REALM + "/protocol/openid-connect/auth", CLIENT_ID, CLIENT_SECRET))
				.build();

		SecurityScheme oauth =
				new OAuthBuilder()
				.name(OAUTH_NAME)
				.grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(scopes()))
				.build();
		return oauth;
	}

	private SecurityContext securityContext() { 
		return SecurityContext.builder()
				.securityReferences( Arrays.asList(new SecurityReference("spring_oauth",
						scopes())))
				.forPaths(PathSelectors.regex(ALLOWED_PATHS))
//				.forPaths(PathSelectors.regex("/ipfs.*", "/dp/*"))
				.build();
	}

	private AuthorizationScope[] scopes() {
		AuthorizationScope[] scopes = { 
				new AuthorizationScope("read:provenance", "for read operations"), 
				new AuthorizationScope("create:provenance", "for write operations"), 
		}; 
		return scopes; 
	} 
}
