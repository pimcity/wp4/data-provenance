package org.imdea.controller;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import org.imdea.dto.JsonString;
import org.imdea.entity.Dataset;
import org.imdea.exception.ErrorObject;
import org.imdea.service.DatasetServiceImpl;

import org.imdea.service.IpfsServiceImpl;
import org.imdea.service.UrlServiceImpl;
import org.imdea.service.WatermarkingServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestHeader;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;

import lombok.AllArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;

import java.io.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.nio.charset.*;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;


/**
 * The Class IpfsController.
 */

/**
 * @author algarecu
 *
 */
@RestController
@RequestMapping("/ipfs")
@AllArgsConstructor
@Tag(name = "ipfs", description = "The IPFS API Endpoints")
@SecurityRequirement(name = "provenance")
public class IpfsController {

	/** The logger. */
    private static final Logger LOGGER = Logger.getLogger(IpfsController.class.getName());

	/** The ipfs service impl. */
	@Autowired
	IpfsServiceImpl ipfsServiceImpl;

	/** The watermarking service impl. */
	@Autowired
	WatermarkingServiceImpl watermarkingServiceImpl;

	@Autowired
	UrlServiceImpl urlService;

	@Autowired
	DatasetServiceImpl datasetServiceImpl;

	/**
	 * Gets the file from IPFS.
	 *
	 * @param the hash ID
	 * @return the file contents
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Operation(summary = "Get a file from IPFS by hash id")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "Found the hash id in IPFS", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = ResponseEntity.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "Invalid hash id supplied in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "Hash id bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@GetMapping(value = "/dataset/{queryId}", produces = "application/json")
	@SecurityRequirement(name = "security_auth")
	public ResponseEntity <?> getOne(@PathVariable("queryId") String hashID) throws IOException {
		byte[] fileContents = ipfsServiceImpl.get(hashID);
		if (fileContents.length == 0) {
			return new ResponseEntity<>(HttpStatus.GONE);
		}
		return new ResponseEntity<>(fileContents, HttpStatus.OK);
	}

	/**
	 * Puts one file into IPFS.
	 *
	 * @param the multipart file
	 * @return the json string
	 * @throws Exception the exception
	 */
	@Operation(summary = "Create new IPFS file")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "Put file to IPFS success", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = JsonString.class)
							)
					}),
			@ApiResponse(
					responseCode = "400",
					description = "File creation bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@PostMapping(value = "/dataset", consumes = MediaType.MULTIPART_FORM_DATA, produces = "application/json")
	@SecurityRequirement(name = "security_auth")
	public ResponseEntity <?> putOne(@RequestPart(value = "file") @Valid @NotNull MultipartFile multipartFile) throws IOException {
		File rawFile = convert(multipartFile);
		String ipfsHashID = ipfsServiceImpl.putFile(rawFile);
		try {
			JsonString ipfsHashIDJsonfied = new JsonString(ipfsHashID);
			return new ResponseEntity<>(ipfsHashIDJsonfied, HttpStatus.OK);
		} catch (Exception exp) {
			return badRequest(exp);
		}
	}

	/**
	 * @param queryId
	 * @return
	 * @throws IOException
	 * @throws CsvDataTypeMismatchException
	 * @throws CsvRequiredFieldEmptyException
	 */
	@Operation(summary = "Send queryId as IPFS hash to watermarkig in DP module")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "QueryId to IPFS success", 
					content = { @Content(
							mediaType = "application/json",
									schema = @Schema(implementation = JsonString.class)
							)
					}),
			@ApiResponse(
					responseCode = "400",
					description = "QueryId bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	})
	@PostMapping(value = "/dataset/{queryId}", produces = "application/json")
	@SecurityRequirement(name = "security_auth")
	public ResponseEntity <?> postQueryId(
			@PathVariable("queryId") String queryId
			) throws IOException {
		byte[] fileContents = ipfsServiceImpl.get(queryId);
		if (fileContents.length == 0) {
			return new ResponseEntity<>(HttpStatus.GONE);
		}

		String path_to_insert_wm = datasetServiceImpl.generateFileFromByteArray(fileContents, queryId);
		LOGGER.log(Level.INFO, "Path to file generated for wm %s", path_to_insert_wm);

		List<String> wmurl = datasetServiceImpl.readGeneratedUrlFile(path_to_insert_wm);

		String[] url_array = wmurl.toArray(new String[0]);

		byte[] key = watermarkingServiceImpl.init_user_key(32);
		System.out.println(Arrays.toString(key)); // printing the private key for testing and wmVerify method usage (temporary)
		
		String[] urlsWM = watermarkingServiceImpl.wmInsert(url_array, key);
		
		try {
			byte [][] listb = new byte[urlsWM.length][];
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
			
			for (int i = 0; i < urlsWM.length; i++) {
				listb[i] = urlsWM[i].getBytes(Charset.defaultCharset());
				outputStream.write(listb[i]);
				outputStream.write("\n".getBytes());
			}
			byte c[] = outputStream.toByteArray();
			
			String filename = queryId;
			String ipfsHashID = ipfsServiceImpl.putByte(filename, c);
			
			// write to local dataset table
			Dataset dataset = new Dataset();
			dataset.setFilePath(path_to_insert_wm);
			dataset.setFileHash(ipfsHashID);
			dataset.setQueryId(queryId);
			dataset.setBuyer_id();
			dataset.setUserId();
			dataset.setTimestamp(LocalDateTime.now());
			datasetServiceImpl.saveDataset(dataset);
			
			JsonString ipfsHashIDJsonfied= new JsonString(ipfsHashID);
			return new ResponseEntity<>(ipfsHashIDJsonfied, HttpStatus.OK);
		} catch (Exception exp) {
			return badRequest(exp);
		}
	}


	/**
	 * Put byte.
	 *
	 * @param Map<String, String> json
	 * @return the json string
	 * @throws Exception the exception
	 */
	@Operation(summary = "Create new IPFS file from bytes")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "New file in bytes to IPFS created", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = JsonString.class)
							)
					}), 
			@ApiResponse(
					responseCode = "400",
					description = "New file in bytes bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@PostMapping(value = "/putByte", consumes = "application/json", produces = "application/json")
	@SecurityRequirement(name = "security_auth")
	public ResponseEntity<?> putByte(@RequestBody Map<String, String> json) throws Exception {
		try {
			String filename = json.get("filename");
			String text = json.get("text");
			String ipfsHashID = ipfsServiceImpl.putByte(filename, text.getBytes());
			JsonString ipfsHashIDJsonfied= new JsonString(ipfsHashID);
			return new ResponseEntity<>(ipfsHashIDJsonfied, HttpStatus.OK);
		} catch (Exception exp) {
			return badRequest(exp);
		}
	}


	/**
	 * Converts a multipart-file to file, 
	 * https://stackoverflow.com/questions/24339990/how-to-convert-a-multipart-file-to-file
	 *
	 * @param file the file
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private ResponseEntity<?> badRequest(Throwable throwable) {
		return new ResponseEntity<>(
				new ErrorObject(), HttpStatus.BAD_REQUEST);
	}
}
