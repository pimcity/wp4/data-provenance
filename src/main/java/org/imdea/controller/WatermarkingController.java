package org.imdea.controller;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.imdea.entity.Dataset;
import org.imdea.exception.ErrorObject;
import org.imdea.service.DatasetServiceImpl;
import org.imdea.service.IpfsServiceImpl;
import org.imdea.service.WatermarkingServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;


@RestController
@RequestMapping("/dp/wm")
@AllArgsConstructor
@Tag(name = "wm", description = "Endpoints for Watermarking verification over datasets")
@SecurityRequirement(name = "provenance")
public class WatermarkingController {
	/** The logger. */
	private static final Logger LOGGER = Logger.getLogger(IpfsController.class.getName());

	/** The ipfs service impl. */
	@Autowired
	IpfsServiceImpl ipfsServiceImpl;

	/** The watermarking service impl. */
	@Autowired
	WatermarkingServiceImpl watermarkingServiceImpl;

	/** The dataset service impl. */
	@Autowired
	DatasetServiceImpl datasetServiceImpl;


	@Operation(summary = "Verify a WM Dataset by ipfs hash")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "dataset belongs to you", 
					content = { @Content(
							schema = @Schema(implementation = ResponseEntity.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "dataset not found",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "dataset does not belong to you",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "410",
					description = "File gone or non existing",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@GetMapping(value = "/{queryId}", produces = "application/json")
	@SecurityRequirement(name = "security_auth")
	public ResponseEntity <?> getOne(
			@PathVariable("queryId") String queryId, 
			@RequestParam(name="key", required = true) byte[] key,
			@RequestParam(name="alpha", required = false, defaultValue = "10") int alpha
					) throws IOException {

		byte[] fileContents = ipfsServiceImpl.get(queryId);
		if (fileContents.length == 0) {
			return new ResponseEntity<>(HttpStatus.GONE);
		}

		String path_to_insert_wm = datasetServiceImpl.generateFileFromByteArray(fileContents, queryId);

		List<String> wmurl = datasetServiceImpl.readGeneratedUrlFile(path_to_insert_wm);

		String[] url_array = wmurl.toArray(new String[0]);
		LOGGER.log(Level.INFO, "Url array for wm verification: " + url_array);


		LOGGER.log(Level.INFO, "Url array to verify wm: " + url_array.toString());
		LOGGER.log(Level.INFO, "Key array to verify wm: " + key);

		Boolean wmResponse = watermarkingServiceImpl.wmVerify(url_array, key, alpha);

		return new ResponseEntity<>(wmResponse, HttpStatus.OK);
	}
}
