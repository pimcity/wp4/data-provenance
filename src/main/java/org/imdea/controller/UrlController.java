package org.imdea.controller;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.util.List;

import org.imdea.entity.Url;
import org.imdea.service.UrlServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(value = "/urls")
@Tag(name = "urls", description = "Endpoints for URL operations")
public class UrlController {

	@Autowired
	UrlServiceImpl urlServiceImpl;

	@Operation(summary = "Create a new url")
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public Url createUrl(@RequestBody Url url) {
		return urlServiceImpl.saveUrl(url);
	}
	
	@Operation(summary = "Get a single url")
	@GetMapping("/{id}")
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "Found the url id", 
					content = { @Content(mediaType = "application/json", 
					schema = @Schema(implementation =  Url.class))}),
			@ApiResponse(responseCode = "400", description = "Invalid request", 
			content = @Content)
			 })
	@ResponseStatus(HttpStatus.OK)
	public Url getUrlById(@PathVariable(value = "id") Long id) {
		return urlServiceImpl.getUrlById(id);
	}
	
	@Operation(summary = "Get 10 urls")
	@GetMapping("/sample")
	public List<Url> getSample() {
		return urlServiceImpl.getSample();
	}


	@Operation(summary = "Get urls for a user id")
	@GetMapping("/user/{userId}")
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "Found the user urls", 
					content = { @Content(mediaType = "application/json", 
					schema = @Schema(implementation =  Url.class))}),
			@ApiResponse(responseCode = "400", description = "Invalid request", 
			content = @Content)
			 })
	@ResponseStatus(HttpStatus.OK)
	public List<Url> getUrlByUserId(@PathVariable(value = "userId") String userId) {
		return urlServiceImpl.getUrlsByUserId(userId);
	}
}