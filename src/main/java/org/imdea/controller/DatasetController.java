package org.imdea.controller;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.io.File;
import java.io.IOException;

import org.imdea.entity.Dataset;
import org.imdea.service.DatasetServiceImpl;
import org.imdea.service.IpfsServiceImpl;

import javax.validation.Valid;
import java.util.List;

import lombok.AllArgsConstructor;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import io.ipfs.api.MerkleNode;
import io.swagger.v3.oas.annotations.tags.Tag;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.imdea.exception.ErrorObject;

@RestController
@RequestMapping("/dp/datasets")
@AllArgsConstructor
@Tag(name = "dataset", description = "Endpoints for CRUD operations on datasets")
public class DatasetController {
	
	@Autowired
	DatasetServiceImpl datasetServiceImpl;
	IpfsServiceImpl ipfsServiceImpl;


	@Operation(summary = "Get All Datasets", description = "Find all datasets", tags = "dataset")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "found datasets", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = Dataset.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "Member not found",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "Bad request",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@GetMapping("")
	public ResponseEntity<List<Dataset>> getAllDatasets() {
		return ResponseEntity.ok(datasetServiceImpl.getDatasets());
	}


	@Operation(
			summary = "POST a dataset",
			description = "CSV file to add for watermarking", 
			tags = "dataset")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "saved dataset in IPFS", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = Dataset.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "Dataset not saved in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "Bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@PostMapping("")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Dataset> generateDataset(@Valid @RequestBody Dataset dataset) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException{
		datasetServiceImpl.saveDataset(dataset);
		datasetServiceImpl.generateDataset(dataset);
		File file = datasetServiceImpl.getFileFromFileSystem(dataset.getFilePath());
		MerkleNode addResult = ipfsServiceImpl.saveFile(file);
		String h = addResult.hash.toString();
		dataset.setFileHash(h);

		try {
			datasetServiceImpl.saveDataset(dataset);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Dataset>(HttpStatus.CREATED);
	}


	@Operation(summary = "Get a Dataset by id")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "Found dataset from id in IPFS", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = Dataset.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "Dataset id not found in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "Dataset id bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/dataset/{id}")
	public ResponseEntity<Dataset> getDatasetById(@PathVariable(value = "id") Integer datasetId) {
		return ResponseEntity.ok(datasetServiceImpl.getDatasetById(datasetId));
	}


	@Operation(summary = "Get a Dataset by queryId")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "Found dataset from queryId in IPFS", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = Dataset.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "Dataset queryId not found in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "Dataset queryId bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@GetMapping("query/{queryId}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ResponseEntity <FileSystemResource> getFile(@PathVariable("queryId") String queryId) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/csv; charset=utf-8");
		return ResponseEntity.ok(new FileSystemResource(datasetServiceImpl.getFileFor(queryId))); 
	}


	@Operation(summary = "Get Dataset for userId")
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200", 
					description = "Got datasets for userId", 
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(implementation = Dataset.class)
							)
					}), 
			@ApiResponse(
					responseCode = "404",
					description = "Dataset for userId not found in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					),
			@ApiResponse(
					responseCode = "400",
					description = "Dataset userId bad request in IPFS",
					content = @Content(
							schema = @Schema(implementation = ErrorObject.class)
							)
					)
	}
			)
	@GetMapping("/user/{userId}")
	public ResponseEntity<List<Dataset>> getDatasetsByUserId(@PathVariable(value = "userId") String userId) {
		return ResponseEntity.ok(datasetServiceImpl.getDatasetsByUserId(userId));
	}    
}