package org.imdea.repository;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.util.List;

import org.imdea.entity.Dataset;
import org.imdea.entity.Url;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UrlRepository extends JpaRepository<Url, Long> {

	@Query(value = "SELECT * FROM url limit 10;", nativeQuery = true)
	List<Url> getSample();
	
	@Query(value = "SELECT * FROM url WHERE id = :id", nativeQuery = true)
	Url getUrlById(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM url WHERE userid = :userId", nativeQuery = true)
	List<Url> getUrlsByUserId(@Param("userId") String string);
	
	@Query(value = "SELECT * FROM url WHERE buyerid = :buyerId", nativeQuery = true)
	List<Url> getUrlsByBuyerId(@Param("buyerId") String string);
	
	@Query(value = "SELECT * FROM url WHERE url IN :queryId", nativeQuery = true)
	List<Url> getUrlsByQueryId(@Param("queryId") String[] urls);
}