package org.imdea.repository;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.util.List;

import org.imdea.entity.Dataset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface DatasetRepository extends JpaRepository<Dataset, Integer> {
	
	@Query(value = "SELECT * FROM dataset WHERE user_id = ?1", nativeQuery = true)
	  List<Dataset> getDatasetsByUserId(String userId);
	
	@Query(value = "SELECT * FROM dataset WHERE queryId = ?1", nativeQuery = true)
	  Dataset getDatasetByQueryId(String queryId);
}