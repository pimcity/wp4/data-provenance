package org.imdea.service;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.util.List;

import org.imdea.entity.Url;
import org.imdea.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UrlServiceImpl implements UrlService {

	@Autowired
	UrlRepository urlRepository;


	@Override
	public Url saveUrl(Url url) {
		return urlRepository.save(url);
	}
	
	@Override
	public Url getUrlById(Long id) {
		return urlRepository.getUrlById(id);
	}

	@Override
	public List<Url> getUrlsByUserId(String userId) {
		return urlRepository.getUrlsByUserId(userId);
	}
	
	@Override
	public List<Url> getUrlsByQueryId(String[] queryId) {
		return urlRepository.getUrlsByQueryId(queryId);
	}
	
	@Override
	public List<Url> getSample() {
		return urlRepository.getSample();
	}
	
	@Override
	public Url getOne(Long id) {
		return urlRepository.getOne(id);
	}
	
//	@Override
//	public List<Url> generatedDatasetFromUrlList(String[] urls) {
//		List<Url> urlListDataset = (List<Url>) new Url();
//		urlListDataset = urlRepository.getUrlsByQueryId(urls);
//		return urlListDataset;
//	}
	
	@Override
	public List<Url> saveUrlList(List<Url> urls) {
		return urlRepository.saveAll(urls);
	}

	@SuppressWarnings("null")
	@Override
	public List<Url> generatedUrlList(String[] urlsWM) {
		List<Url> url_list = null;
		
		for (int i=0; i<urlsWM.length; i++) {
			Url url = new Url();
			url.setUrl(urlsWM[i]);
			url_list.add(url);
		}
		return url_list;
	}
}
