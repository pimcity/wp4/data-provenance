package org.imdea.service;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.util.BitSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.imdea.controller.IpfsController;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;

@Service
public class WatermarkingServiceImpl implements WatermarkingService {
	/** The logger. */
    private static final Logger LOGGER = Logger.getLogger(IpfsController.class.getName());
    
	/*
	 * @input: secParam is the security parameter. 
	 * How long the key should be (e.g. 32 byte)
	 * @return generates a random value as the wm key
	 */
	@Override
	public byte[] keyGen(int secParam) {
		byte[] nonce = new byte[secParam];
		new SecureRandom().nextBytes(nonce);
		return nonce;

	}

	/*
	 *@input url is the list of urls to be watermarked and the key is wm key in byte format
	 *@return returns the wm list of urls.
	 */
	@Override
	public String[] wmInsert(String[] urls_list, byte[] key) {
		MessageDigest digest;
		String keys = new String(key);
		String[] wm_url = new String[urls_list.length];
		try {
			digest = MessageDigest.getInstance("SHA-256");

			for (int i = 0; i < urls_list.length; i++) {
				wm_url[i] = "";
				String hashIn = keys + urls_list[i];
				byte[] encodedhash = digest.digest(hashIn.getBytes());
				BitSet bitset = BitSet.valueOf(encodedhash);

				for (int j = 0; j < urls_list[i].length(); j++) {

					// If the char is lower-case
					if (urls_list[i].charAt(j) >= 97 && urls_list[i].charAt(i) <= 122) {
						if (bitset.get(j)) {
							wm_url[i] += (char) (urls_list[i].charAt(j) - 32);
						} else {
							wm_url[i] += urls_list[i].charAt(j);
						}
					}
					// If the char is upper-case
					else if (urls_list[i].charAt(j) >= 65 && urls_list[i].charAt(i) <= 90) {
						if (bitset.get(j)) {
							wm_url[i] += (char) (urls_list[i].charAt(j) + 32);
						} else {
							wm_url[i] += urls_list[i].charAt(j);
						}
					} else {
						wm_url[i] += urls_list[i].charAt(j);
					}
				}
				hashIn = "";
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return wm_url;
	}

	/*
	 * @input url is a wm-url and bitsequence which is the H(rand||url)
	 * @return if wm-url is verified by the given key
	 */
	@Override
	public boolean url_verify(String url, BitSet bitset) {

		for (int j = 0; j < url.length(); j++) {

			// If the char is lower-case
			if (url.charAt(j) >= 97 && url.charAt(j) <= 122) {
				if (bitset.get(j)) {
					return false;
				}
			}
			// If the char is upper-case
			else if (url.charAt(j) >= 65 && url.charAt(j) <= 90) {
				if (bitset.get(j)==false) {
					return false;
				} 
			}
		}
		return true;
	}


	/* wmVerify is a verification method
	 * 
	 * 
	 * @param url: list of given urls to watermark. 
	 * @param key: wm private key generated during insertion of wm.
	 * @param alpha: alpha is the verification threshold set.
	 * @return: true if given list of watermarked urls are verified using the key and alpha, return false if not verified.
	 */
	@Override
	public boolean wmVerify(String[] url, byte[] key, int alpha) {
		MessageDigest digest;
		String keys = new String(key);
		
		int count = 0;
		try {
			digest = MessageDigest.getInstance("SHA-256");

			for (int i = 0; i < url.length; i++) {
				String tr = url[i].toLowerCase();
				String hashIn = keys + tr;
				byte[] encodedhash = digest.digest(hashIn.getBytes());
				BitSet bitset = BitSet.valueOf(encodedhash);
				
				for (int j = 0; j < url.length; j++) {
					if (url_verify(url[j], bitset)) {
						count++;
						LOGGER.log(Level.INFO, "Count of verify wm: " + count);
					}
				}
				hashIn = "";
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		LOGGER.log(Level.INFO, "Count total of verify wm: " + count);
		if (count >= alpha)
			return true;
		else
			return false;
	}

	@Override
	public boolean storeKey(byte[] key) {
		return false;
	}
	
	@Override
	public String[] init_url_list() {
		String [] urls = { "www.facebook.com", "www.google.com", "www.twitter.com" };
		return urls;
	}
	
	@Override
	public byte[] init_user_key(int lenght) {
		byte[] nonce = keyGen(32);
		return nonce;
	}
	
	@Override
	public void print_url_watermarked_insert(String[] wmurl, byte[] nonce) {
		System.out.println("The result is : " + wmVerify(wmurl, nonce, 3));
		return;
	}
	
	@Override
	public void print_url_watermarked_verify(String[] wmurl) {
		for (int i = 0; i < wmurl.length; i++) {
			System.out.println(wmurl[i]);
		}
		return;
	}
}
