package org.imdea.service;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS

#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.nio.file.Files;
import java.nio.file.Paths;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.BufferedReader;

import org.imdea.entity.Dataset;
import org.imdea.exception.ResourceNotFoundException;
import org.imdea.repository.DatasetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;


@Service
public class DatasetServiceImpl implements DatasetService {

	@Autowired
	DatasetRepository datasetRepository;

	@Override
	public Dataset saveDataset(Dataset dataset) {
		return datasetRepository.save(dataset);
	}

	@Override
	public List<Dataset> getDatasets() {
		return datasetRepository.findAll();
	}

	@Override
	public Dataset getDatasetById(@PathVariable(value = "id") Integer datasetId) {
		return datasetRepository.findById(datasetId)
				.orElseThrow(() -> new ResourceNotFoundException("Dataset", "id", datasetId));
	}

	@Override
	public List<Dataset> getDatasetsByUserId(String userId) {
		return datasetRepository.getDatasetsByUserId(userId);
	}

	public Dataset generateDataset(Dataset dataset) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		Dataset urlListDataset = new Dataset(); 
		urlListDataset = datasetRepository.getDatasetByQueryId(dataset.getQueryId());
		String dataset_formatted_id = String.format("%05d", dataset.getQueryId());
		String filePath = "/tmp/"+dataset_formatted_id+"_"+Instant.now().toEpochMilli()+".csv";

		try (
				Writer writer = Files.newBufferedWriter(Paths.get(filePath));

				CSVWriter csvWriter = new CSVWriter(writer,
						CSVWriter.DEFAULT_SEPARATOR,
						CSVWriter.NO_QUOTE_CHARACTER,
						CSVWriter.DEFAULT_ESCAPE_CHARACTER,
						CSVWriter.DEFAULT_LINE_END); ){

			StatefulBeanToCsv<Dataset> beanToCsv = new StatefulBeanToCsvBuilder<Dataset>(writer)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();

			beanToCsv.write(urlListDataset);
		};

		dataset.setFilePath(filePath);;	 
		return dataset;	
	}

	@Override
	public String generateFileFromByteArray(byte[] filecontent, String queryId) throws IOException {
		//Write byte array to csv file locally in /tmp/		
		String filepath = "/tmp/"+queryId+"_"+Instant.now().toEpochMilli();

		try (FileOutputStream fos = new FileOutputStream(filepath)) {
			fos.write(filecontent);
			fos.close();
		} catch (FileNotFoundException ex) {
			System.out.println("File not found exception");
			ex.printStackTrace();
		}
		System.out.println("Write of IPFS bytes to filesystem complete.");
		return filepath;
	}

	@Override
	public List<String> readGeneratedUrlFile(String filepath) throws FileNotFoundException {
		List<String> url_list = new ArrayList<String>();
		BufferedReader reader = null;
		
		try {
			String strCurrentLine;
			reader = new BufferedReader(new FileReader(filepath));
			try {
				while ((strCurrentLine = reader.readLine()) != null) {
					if (strCurrentLine != null) {
						url_list.add(strCurrentLine);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException ex) {
			System.out.println("File not found exception");
			ex.printStackTrace();
		} finally {

			try {
				if (reader != null)
					reader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		// local garbage collector deletes old files
		try {
            Files.delete(Paths.get(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return url_list;
	}

	public File getFileFromFileSystem(String filePath) throws IOException {
		File file = new File(filePath);
		return file;	
	}


	public Dataset getDatasetByHash(String fileHash) { 
		return datasetRepository.getDatasetByQueryId(fileHash); }


	public String getFileFor(String fileHash) {
		Dataset dataset = getDatasetByHash(fileHash);
		String filePath = dataset.getFilePath();
		return filePath;
	}
}
