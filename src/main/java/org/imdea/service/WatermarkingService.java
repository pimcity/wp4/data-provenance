package org.imdea.service;

import java.util.BitSet;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

public interface WatermarkingService {
	public String[] wmInsert(String [] url, byte[] key);
	public boolean wmVerify(String[] url, byte[] key, int alpha);
	public boolean storeKey(byte[] key);
	public byte[] keyGen(int secParam);
	public byte[] init_user_key(int lenght);
	public boolean url_verify(String url, BitSet bitset);
	public void print_url_watermarked_insert(String[] wmurl, byte[] nonce);
	public void print_url_watermarked_verify(String[] wmurl);
	public String[] init_url_list();
}