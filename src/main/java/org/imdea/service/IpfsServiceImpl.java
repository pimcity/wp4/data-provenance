package org.imdea.service;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * The Class IpfsServiceImpl.
 */
@Service
public class IpfsServiceImpl {

    /**
     * Gets IPFS initialized.
     *
     * @return the ipfs
     */
    public IPFS getIpfs() {
        return ipfs;
    }
    
    IPFS ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
    
    /** The logger. */
    protected Logger logger = Logger.getLogger(IpfsServiceImpl.class.getName());
    
    
    /**
     * Put a file to IPFS.
     *
     * @param file is the file
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MerkleNode saveFile(File file) throws IOException {
    	logger.info("IpfsServiceImpl put() invoked: for " + file);
        NamedStreamable.FileWrapper fileWrapper = new NamedStreamable.FileWrapper(file);
        MerkleNode addResult = ipfs.add(fileWrapper).get(0);
        logger.info("IpfsServiceImpl put() result: " + addResult.toJSONString());
        return addResult;
    }
    
    
    public String putFile(File file) throws IOException {
    	MerkleNode addResult = saveFile(file);
        return addResult.toJSONString();
    }
    
    
    /**
     * Put a byte[] to IPFS.
     *
     * @param file is byte array []
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String putByte(String filename, byte[] file) throws IOException {
        NamedStreamable.ByteArrayWrapper fileWrapper = new NamedStreamable.ByteArrayWrapper(filename, file);
        MerkleNode addResult = ipfs.add(fileWrapper).get(0);
        logger.info("IpfsServiceImpl put() result: " + addResult.toJSONString());
        return addResult.toJSONString();
    }

    /**
     * Gets the file from IPFS.
     *
     * @param hashStr is the hash the file, e.g., Qm... (for SHA-256)
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public byte[] get(String hashStr) throws IOException {
        Multihash filePointer = Multihash.fromBase58(hashStr);
        byte[] fileContents = ipfs.cat(filePointer);
        logger.info("IpfsServiceImpl get() for " + hashStr);
        return fileContents;
    }

}