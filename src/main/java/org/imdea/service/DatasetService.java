package org.imdea.service;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS

#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import java.util.List;

import org.imdea.entity.Dataset;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public interface DatasetService {
	public abstract Dataset saveDataset(Dataset dataset);
	public abstract Dataset getDatasetById(Integer id);
	public abstract List<Dataset> getDatasets();
	public abstract List<Dataset> getDatasetsByUserId(String userId);
	public abstract String generateFileFromByteArray(byte[] filecontent, String queryId) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException;
	public abstract List<String> readGeneratedUrlFile(String filepath) throws FileNotFoundException;
}
