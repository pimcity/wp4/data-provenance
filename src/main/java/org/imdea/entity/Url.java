package org.imdea.entity;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.EntityListeners;
import javax.persistence.TemporalType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "url")
@EntityListeners(AuditingEntityListener.class)
public class Url implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7907274104353207680L;

	/*
	 * Fields
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Schema(
            description = "The primary key",
            example = "0001"
    )
    private Long id;
	
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Schema(
            description = "The timestamp column in table url",
            example = "2016-11-05 19:28:49"
    )
	private Date timestamp;
	
	
	@Column(columnDefinition="TEXT")
	@Schema(
            description = "The url column in table url",
            example = "http://www.ant1iwo.com/"
    )
    private String url;
	
	@Column(columnDefinition="TEXT")
	@Schema(
            description = "The user id column in table url",
            example = "269y56hakj5s04oog4"
    )
    private String userid;
	
	@Column(columnDefinition="TEXT")
	@Schema(
            description = "The buyer id column in table url",
            example = "a3w34w3"
    )
    private String buyerid;
	
	
	/*
	 * Url setters and getters
	 */
	
	public Long getId() {
		return id;
	}
	

	public Date getTimestamp() {
		return timestamp;
	}
	
	public String getUrl() {
		return url;
		
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	} 

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userid;
	}


	public void setUserId(String userId) {
		this.userid = userId;
	}

	public String getBuyerid() {
		return buyerid;
	}

	public void setBuyerid(String buyerid) {
		this.buyerid = buyerid;
	}
	
}