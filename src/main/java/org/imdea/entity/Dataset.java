package org.imdea.entity;

/**
 * 
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Provenance framework
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
 */

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.data.annotation.CreatedDate;
import java.io.Serializable;
import java.time.*;
import java.util.Random;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "dataset")
@EntityListeners(AuditingEntityListener.class)
public class Dataset implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4620892243483143121L;

	@Schema(
            description = "Dataset unique identifier id",
            example = "1",
            required = true
    )
	@Id
	@Column(name="query_id")
    private String queryId;
	
    @Schema(
            description = "The user id column in table url or dataset",
            example = "123456"
    )
	@Column(name="user_id", columnDefinition="INTEGER")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer user_id;
    
    @Schema(
            description = "The buyer id column in table url or dataset",
            example = "12345"
    )
	@Column(name="buyer_id", columnDefinition="INTEGER")
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer buyer_id;
    
    @Schema(
            description = "",
            example = ""
    )
    private String filePath;
    
    @Schema(
            description = "",
            example = ""
    )
    private String fileHash;
    
    @Schema(
            description = "",
            example = ""
    )
	@Column(name="timestamp", updatable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @CreatedDate
    private LocalDateTime timestamp;
    

	// Setters and Getters 
	public Integer getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id() {
		this.buyer_id = new Random().nextInt(20);
	}

	public String getQueryId() {
		return queryId;
	}
	
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public Integer getUserId() {
		return user_id;
	}

	public void setUserId() {
		this.user_id = new Random().nextInt(10000);
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public String getDatasetByQueryId() {
		return fileHash;
	}

	public void setFileHash(String fileHash) {
		this.fileHash = fileHash;
	}
	
	public String generateHashFromFilePath(String filePath) {
		return DigestUtils.sha256Hex(filePath);		
	}
}