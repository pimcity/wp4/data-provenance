#!/bin/bash

#Restart instructions for Data Provenance in Dev Environment
Then, ssh 'portability.pimcity-h2020.eu' and get user 'agarcia'

ssh agarcia@portability.pimcity-h2020.eu
cd ~/data-provenance

# Kill existing screen session:
screen -XS "data-provenance" quit

#Create a new screen session:
screen -S "data-provenance" -d -m

# Attach to new session
screen -r "data-provenance"

# list of exported variables required for provenance to run
export JAVA_HOME='/usr/lib/jvm/java-11-openjdk-amd64'

# Make sure IPFS daemon is ready
ipfs daemon 2>/dev/null | grep -i -o -m1 'Daemon is ready' & tail -f --pid=$! /dev/null

# run it
mvn spring-boot:run

#Then wait a couple minutes and check that everything is working fine with:
curl https://easypims.pimcity-h2020.eu/provenance/actuator/health
